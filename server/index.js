import express from 'express';
import morgan from 'morgan';
import path from 'path';
import fs from 'fs';
import xmldom from 'xmldom';

const esm = require('express-status-monitor');

const xmlparser = require('express-xml-bodyparser');

const passwords = require('./passwords.json');

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });

export default (app) => {
  app.use(esm());
  app.use(express.json());
  app.use(morgan('combined', { stream: accessLogStream }));


  app.get('/foo', (req, res) => {
    console.log(req.ip);
    res.json({ msg: 'foo' });
  });

  app.get('/bar', (req, res) => {
    const parser = new xmldom.DOMParser();
    parser.parseFromString(path.join(__dirname, 'bomb.xml'), 'text/xml');
    res.send('lol');
  });

  app.post('/bomb', xmlparser({ trim: true }), (req, res) => {
    // const parser = new xmldom.DOMParser();
    // const xml = parser.parseFromString(req.body, 'text/xml');
    // const text = new xmldom.XMLSerializer(xml);
    // res.send(typeof text);
    console.log(req.body);
    res.send(req.body);
  });

  app.post('/easylogin', (req, res) => {
    if (req.body.password === passwords[0]) res.send('HERZLICHEN GLÜCKWUNSCH!\nPasswort geknackt');
    else res.send('Falsches Passwort!');
  });

  app.post('/login', (req, res) => {
    let j = {};
    fs.readFile(path.join(__dirname, 'logs.json'), (err, data) => {
      if (err) {
        console.log(err);
      } else {
        j = JSON.parse(data);
        
        if (!j[req.ip] || j[req.ip].fails < 3 || new Date().getTime() - j[req.ip].timestamp >= 60000) {
          if (req.body.password === passwords[1]) {
            j[req.ip].timeOfSuccess = new Date().getTime();
            res.send('HERZLICHEN GLÜCKWUNSCH!\nPasswort geknackt');
          } else if (j[req.ip]) {
            let fails = j[req.ip].fails;
            if (j[req.ip].fails >= 3) fails = 0;
            j[req.ip] = { fails: fails + 1, timestamp: new Date().getTime(), passwords: j[req.ip].passwords.concat([req.body.password]) };
            res.send('Falsches Passwort');
          } else {
            j[req.ip] = { fails: 1, timestamp: new Date().getTime(), passwords: [req.body.password] };
            res.send('Falsches Passwort');
          }
        } else {
          j[req.ip] = { fails: j[req.ip].fails + 1, timestamp: new Date().getTime(), passwords: j[req.ip].passwords.concat([req.body.password]) };
          res.send('Zu viele Fehlversuche! Bitte 1 Minute warten.');
        }
        
        // if (j[req.ip]) {
        //   if (j[req.ip].fails >= 3) {
        //     if (new Date().getTime() - j[req.ip].timestamp >= 60000) {
        //       j[req.ip].fails = 1;
        //       res.send('Falsches Passwort');
        //     } else {
        //       res.send('zu viele fehlversuche, bitte 1 min warten');
        //     }
        //   } else {
        //     j[req.ip] = { fails: j[req.ip].fails + 1, timestamp: new Date().getTime(), passwords: j[req.ip].passwords.concat([req.body.password]) };
        //     res.send('Falsches Passwort');
        //   }
        // } else {
        //   j[req.ip] = { fails: 1, timestamp: new Date().getTime(), passwords: [req.body.password] };
        // }
        // console.log(j);
        fs.writeFile(path.join(__dirname, 'logs.json'), JSON.stringify(j), (error) => {
          console.log(error || 'log changed');
        });
      }
    });
  });
};
